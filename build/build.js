var gui = new dat.GUI();
var params = {
    Download_Image: function () { return save(); },
    random_seed: 0,
};
gui.add(params, "Download_Image");
var ai = new rw.HostedModel({
    url: "https://biggan-09ae3b83.hosted-models.runwayml.cloud/v1/",
    token: "yWPrDt5EDnWCyS1vkakbCg==",
});
var ai_bis = new rw.HostedModel({
    url: "https://stylegan-bd651a38.hosted-models.runwayml.cloud/v1/",
    token: "ryzXif6A/4gB10dLtkdNfQ==",
});
var img;
var JellyfishJson = [];
var NebulasJson = [];
var z = [];
var z_bis = [];
var categories = ['nebulas', 'jellyfish', 'bubble'];
var frameNB = 0;
var maxFrames = 300;
var compteur = 13;
function draw() {
    if (img) {
        randomSeed(0);
        noStroke();
        translate(width / 2, height / 2);
        image(img, -width / 2, -height / 2, width, height);
        for (var i = 0; i < 200; i++) {
            var angle = random(TWO_PI);
            var radius = randomGaussian(0, width * 0.8);
            fill(0, random(100), random(255), 15);
            ellipse(radius * cos(angle), radius * sin(angle), 300);
        }
    }
}
function downloadFrame() {
    if (frameNB <= maxFrames) {
        var filename = nf(frameNB, 3, 0) + ".png";
        save(filename);
        frameNB++;
    }
}
function makeRequest(categorie) {
    if (compteur > 0) {
        if (categorie == 'nebulas') {
            var inputs = {
                "z": z_bis,
                "truncation": 0.5,
            };
            ai_bis.query(inputs).then(function (outputs) {
                var image = outputs.image;
                img = createImg(image);
                img.hide();
                z_bis[0] += 0.1;
                downloadFrame();
                compteur--;
                makeRequest(categorie);
            });
        }
        else {
            var inputs = {
                "z": z,
                "category": categorie
            };
            ai.query(inputs).then(function (outputs) {
                var generated_output = outputs.generated_output;
                img = createImg(generated_output);
                img.hide();
                z[0] += 0.1;
                downloadFrame();
                compteur--;
                makeRequest(categorie);
            });
        }
    }
    else {
        compteur = 13;
    }
}
function setup() {
    p6_CreateCanvas();
    var rand1 = random(0, 10);
    var rand2 = random(0, 16);
    var randCategorie = random(categories);
    console.log(randCategorie);
    for (var i = 0; i < 128; i++) {
        z[i] = JellyfishJson[int(rand1)][i];
    }
    for (var i = 0; i < 512; i++) {
        z_bis[i] = NebulasJson[21][i];
    }
    makeRequest('nebulas');
}
function windowResized() {
    p6_ResizeCanvas();
}
function preload() {
    for (var i = 0; i <= 21; i++) {
        var filepath = "./image/nebulas" + nf(i, 2, 0) + ".json";
        NebulasJson[i] = loadJSON(filepath, []);
    }
    for (var i = 0; i <= 13; i++) {
        var filepath = "./image/jellyfish" + nf(i, 2, 0) + ".json";
        JellyfishJson[i] = loadJSON(filepath, []);
    }
}
var __ASPECT_RATIO = 1;
var __MARGIN_SIZE = 25;
function __desiredCanvasWidth() {
    var windowRatio = windowWidth / windowHeight;
    if (__ASPECT_RATIO > windowRatio) {
        return windowWidth - __MARGIN_SIZE * 2;
    }
    else {
        return __desiredCanvasHeight() * __ASPECT_RATIO;
    }
}
function __desiredCanvasHeight() {
    var windowRatio = windowWidth / windowHeight;
    if (__ASPECT_RATIO > windowRatio) {
        return __desiredCanvasWidth() / __ASPECT_RATIO;
    }
    else {
        return windowHeight - __MARGIN_SIZE * 2;
    }
}
var __canvas;
function __centerCanvas() {
    __canvas.position((windowWidth - width) / 2, (windowHeight - height) / 2);
}
function p6_CreateCanvas() {
    __canvas = createCanvas(__desiredCanvasWidth(), __desiredCanvasHeight());
    __centerCanvas();
}
function p6_ResizeCanvas() {
    resizeCanvas(__desiredCanvasWidth(), __desiredCanvasHeight());
    __centerCanvas();
}
var p6_SaveImageSequence = function (durationInFrames, fileExtension) {
    if (frameCount <= durationInFrames) {
        noLoop();
        var filename_1 = nf(frameCount - 1, ceil(log(durationInFrames) / log(10)));
        var mimeType = (function () {
            switch (fileExtension) {
                case 'png':
                    return 'image/png';
                case 'jpeg':
                case 'jpg':
                    return 'image/jpeg';
            }
        })();
        __canvas.elt.toBlob(function (blob) {
            p5.prototype.downloadFile(blob, filename_1, fileExtension);
            setTimeout(function () { return loop(); }, 100);
        }, mimeType);
    }
};
//# sourceMappingURL=../src/src/build.js.map