// -------------------
//  Parameters and UI
// -------------------

const gui = new dat.GUI()
const params = {
    Download_Image: () => save(),
    random_seed : 0,
}
gui.add(params, "Download_Image")

// You need to change this and use the values given to you by Runway for your own model
const ai = new rw.HostedModel({
    url: "https://biggan-09ae3b83.hosted-models.runwayml.cloud/v1/",
    token: "yWPrDt5EDnWCyS1vkakbCg==",
});

const ai_bis = new rw.HostedModel({
    url: "https://stylegan-bd651a38.hosted-models.runwayml.cloud/v1/",
    token: "ryzXif6A/4gB10dLtkdNfQ==",
});

let img: p5.Element

let JellyfishJson = [] //tableau des json selectionnés pour les meduses
let NebulasJson = [] //tableau des json selectionnés pour les nébuleuses
let z = []
let z_bis = []
let categories = ['nebulas','jellyfish', 'bubble']
let frameNB = 0 //compteur du nombre d'image enregistrée
let maxFrames = 300 //nombre maximum d'image enregistrée
let compteur = 13 //compteur du nombre de frames par séquence d'image d'une catégorie

// -------------------
//       Drawing
// -------------------

function draw() {
      
    if (img)
    {
        randomSeed(0)
        noStroke()
        translate (width/2, height/2)
        image(img, -width/2, -height/2, width, height)
        // Filtre bleu
        for (let i = 0; i < 200; i++) {
            const angle = random(TWO_PI)
            const radius = randomGaussian(0, width * 0.8)
            fill(0, random(100), random(255), 15)
            ellipse(radius * cos(angle), radius * sin(angle), 300)
        }
    }
}

function downloadFrame()
{
    if(frameNB <= maxFrames){
        let filename = nf(frameNB, 3, 0) + ".png"
        save(filename);
        frameNB++;
    }
}

function makeRequest(categorie){

    if (compteur > 0)
    {
        if (categorie == 'nebulas')
        {
            const inputs = {
                "z": z_bis,
                "truncation": 0.5,
            };
            ai_bis.query(inputs).then(outputs => {
                const { image } = outputs;
                img = createImg(image)
                img.hide()
                z_bis[0] += 0.1
                downloadFrame()
                //appel recursif avec la même catégorie jusqu'à ce que le compteur soit égal à 0
                compteur--
                makeRequest(categorie)
            });
        }
        else
        {
            const inputs = {
                "z": z,
                "category": categorie
            };
            ai.query(inputs).then(outputs => {
                const { generated_output } = outputs;
                img = createImg(generated_output)
                img.hide()
                z[0] += 0.1
                downloadFrame()
                //appel recursif avec la même catégorie jusqu'à ce que le compteur soit égal à 0
                compteur--
                makeRequest(categorie)
            });
        }
    }
    else 
    {
        compteur = 13
    }
}

// -------------------
//    Initialization
// -------------------

function setup() {
    p6_CreateCanvas()

    for (let k = 0; k < 10; k++)
    {
        let rand1 = random(0,10)
        let rand2 = random(0,16)
        let randCategorie = random(categories)

        for (let i = 0; i < 128; i++) {
            z[i] = JellyfishJson[int(rand1)][i] //on récupère un json aléatoire parmi ceux selectionnés
        }
        for (let i = 0; i < 512; i++) {
            z_bis[i] = NebulasJson[int(rand2)][i] //on récupère un json aléatoire parmi ceux selectionnés
        }
       
        makeRequest(randCategorie)
    }
}

function windowResized() {
    p6_ResizeCanvas()
}


function preload() {

    //on stock dans un tableau les json selectionnés

    for (let i = 0; i <= 21; i++)
    {
        let filepath = "./image/nebulas" + nf(i, 2, 0) + ".json"
        NebulasJson[i] = loadJSON(filepath, [])
    }

    for (let i = 0; i <= 13; i++)
    {
        let filepath = "./image/jellyfish" + nf(i, 2, 0) + ".json"
        JellyfishJson[i] = loadJSON(filepath, [])
    }
}